class SessionController < ApplicationController
  def new
    render :login
  end

  def create
    matching_users = User.where(:username => params[:username], :password => params[:password])
    if not matching_users.empty?
      redirect_to tasks_path
    else
      redirect_to login_path, :flash => { :error => 'Authentication failed' }
    end
  end

  def destroy
    redirect_to login_path, :flash => { :notice => 'You have been logged out' }
  end
end
