require 'spec_helper'

feature 'Session management' do
  scenario 'Log in is valid for registered users' do
    user = User.create(:username => 'user', :password => 'password')

    visit login_path
    fill_in 'Username', :with => user.username
    fill_in 'Password', :with => user.password
    click_button 'Log in'

    expect(page).to have_content('Listing tasks')
  end

  scenario 'Log in is invalid when password is incorrect' do
    user = User.create(:username => 'user', :password => 'password')

    visit login_path
    fill_in 'Username', :with => user.username
    fill_in 'Password', :with => 'wrong password'
    click_button 'Log in'

    expect(page).to have_content('Authentication failed')
  end

  scenario 'Log out closes the session' do
    user = User.create(:username => 'user', :password => 'password')

    visit login_path
    fill_in 'username', :with => user.username
    fill_in 'Password', :with => user.password
    click_button 'Log in'
    click_link 'Log out'

    expect(page).to have_content('You have been logged out')
  end
end
