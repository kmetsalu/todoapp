Todoapp::Application.routes.draw do
  resources :tasks

  get '/login' => 'session#new', :as => 'login'
  post '/login' => 'session#create', :as => 'login'
  get '/logout' => 'session#destroy', :as => 'logout'
end
